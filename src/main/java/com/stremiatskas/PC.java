package com.stremiatskas;


public class PC 
{
    public static void main( String[] args )
    {
        AMD AMD = new AMD();
        Class AMD1 = AMD.getClass();
        CPU AMD2 = (CPU) AMD1.getAnnotation(CPU.class);
        System.out.println(AMD2.cores());

        Intel Intel = new Intel();
        Class Intel1 = Intel.getClass();
        CPU Intel2 = (CPU) Intel1.getAnnotation(CPU.class);
        System.out.println(Intel2.cores());
    }
}